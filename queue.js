let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(item){
    collection.push(item);
}

function dequeue(){
   if(collection.length > 0) { 
        return collection.shift();
         } else {
             return false
         }
}

function front(){
    if(collection.length > 0) {
              return collection[0];
          } else {
              return false
          }
}

function size(){
    return collection.length;

}

function isEmpty(){
    if(collection.length > 0){
        return false
    } else { 
        return true
    }
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};